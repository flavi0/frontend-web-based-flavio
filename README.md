# **NEPPO - Teste Frontend Web Based**

# Projeto desenvolvido:

O projeto foi desenvolvido em duas etapas:

A primeira envolvendo a construção de uma **web API RESTful** desenvolvida em **ASP.NET Core** e **Entity Framework Core 2.0**. 

A segunda parte consistiu no desenvolvimento de uma aplicação front-end utilizando Angular5, Html5, Css3 e Bootstrap 4.

# Execução (dicas):
Para rodar a aplicação, é necessário executar o projeto **PersonRegister** e suas respectivas dependências na porta **8090**. Para isso, é necessário passar a conexão da base de dados como variável de ambiente, contendo o nome da base e o parâmero `MultipleActiveResultSets` setado como`true`. 

Exemplo:

        "CONNECTION_STRING": "Data Source=DESKTOP-P6NLD0E;Initial Catalog=PersonDb;Integrated Security=True;MultipleActiveResultSets=True"

Após executar o backend, é preciso executar os códigos que estão dentro da pasta **dist** no repositório.